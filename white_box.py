class WhiteBox:
    @staticmethod
    def compute(n: int, array: list[float]) -> int:
        assert (n == len(array))                            # 1
        result = 0                                          # 2
        for index in range(len(array)-1):                   # 3
            if array[index] >= 0 and array[index+1] >= 0:   # 4
                result += 1                                 # 5
        return result                                       # 6
