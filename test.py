import unittest

from white_box import WhiteBox
from parameterized import parameterized


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.white_box = WhiteBox()

    @parameterized.expand([
        (1, [1.2], 0),  # ТВ_2, (true,=,,)
        (0, [], 0),
        (3, [1.1, 2.1, 3.4], 2),  # ТВ_4, (true,<,≥,≥)
        (3, [1.7, -2.1, 3.2], 0),  # ТВ_3, (true,<,≥,<)
        (3, [-1.3, 2.3, 3.6], 1),  # (true,<,<,≥)
        (3, [-1.3, -2.3, -3.6], 0),  # (true,<,<,<)
        (4, [-1.7, 0, 0.5, 1.1], 2),
    ])
    def test_correct_value(self, n, array, expected):
        print("test_correct_value", end=' ')
        result = self.white_box.compute(n, array)
        print(result)
        self.assertEqual(expected, result)

    @parameterized.expand([
        (3, [1.1, 2.1]),  # ТВ_1, (false,,,)
    ])
    def test_error_len(self, n, array):
        self.assertRaises(AssertionError, lambda: self.white_box.compute(n, array))


if __name__ == '__main__':
    unittest.main()
